#!/bin/bash

docker run --rm -v $(pwd):/mnt:rw --entrypoint "" linuxserver/libreoffice bash -c 'cd /mnt && lowriter --headless --convert-to pdf matthew-cane-cv.odt'